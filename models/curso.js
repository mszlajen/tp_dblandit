const mongoose = require("mongoose")

const Cliente = require("./cliente")

const Curso = new mongoose.Schema({
    agnio: {type: Number},
    duracion: {type: Number},
    tema: {type: String},
    alumnos: {type: [Cliente]}
}, {collection: 'cursos'})

module.exports = mongoose.model("Curso", Curso)