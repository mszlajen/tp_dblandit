export default (context) =>  
        (event) => {
          const {name, value} = event.target
          if(!isNaN(value))
            context.setState({[name]: value})
        }