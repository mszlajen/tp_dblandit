import React from 'react'
import {connect} from 'react-redux'
import deleteCurse from '../redux/actions/deleteCurse'
import { Button, Container, Typography } from '@material-ui/core'

const mapDispatchToProps = dispatch => {
  return {
    delete: (id) => dispatch(deleteCurse(id))
  }
}

class EliminarCurso extends React.Component {
  handleYes = () => {  
      fetch(`/cursos/${this.props.data._id}`, {method: "DELETE"})
      .then(result => result.json())
      .then(result => this.props.delete(result.message._id))
    }

  render() {
    return (
      <Container>
        <Typography>Está seguro que desea borrarlo?</Typography>
        <br/>
        <Button variant="contained" onClick={this.handleYes}>Sí</Button>
        <Button name={this.props.cancelName} variant="contained" onClick={this.props.callback}>Cancelar</Button>
      </Container>
    )
  }
}

export default connect(() => {}, mapDispatchToProps)(EliminarCurso)