import saveCurses from '../redux/actions/saveCurses'

const toString = function(o) {
  const keys = Object.keys(o)
  const params = []
  keys.forEach(k => o[k] !== ""? params.push(`${k}=${o[k]}`) : "")
  return params.reduce((acc, cur) => `${cur}&${acc}`, "")
}

export default (
  function(dispatch, queryParams = {}) {
    const params = toString(queryParams)
    fetch(`/cursos/?${params}`)
    .then(resultado => resultado.json())
    .then(resultado => dispatch(saveCurses(resultado.message)))
    }
)