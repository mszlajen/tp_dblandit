export default (
  function(state=[], action){
    switch(action.type) {
      case "SAVE":
        return action.payload
      case "DELETE":
        return state.filter(curse =>  curse._id !== action.payload)
      case "APPEND":
        return state.map(curse => {
          if(curse._id !== action.payload.curse) 
            return curse
          const newStudents = curse.alumnos.splice(0)
          newStudents.push(action.payload.student)
          return {
            ...curse,
            alumnos: newStudents
          }
        })
      default:
        return state
    }
  }
)