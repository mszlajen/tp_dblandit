import {combineReducers} from 'redux'
import windowReducer from './window'
import cursesReducer from './curses'
import stateReducer from './state'

const allReducers = combineReducers({
  window: windowReducer,
  curses: cursesReducer,
  state: stateReducer
})

export default allReducers