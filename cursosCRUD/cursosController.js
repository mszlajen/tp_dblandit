const Curso = require('../models/curso')
const { validationResult } = require('express-validator')

const getCursos = (req, res, next) => {
  const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({
            code: 10,
            message: errors.array()
        })
    }

  const pagina = req.query.pagina || 0
  const query = {}
  if('duracion' in req.query)
    query.duracion = req.query.duracion
  if('agnio' in req.query)
    query.agnio = req.query.agnio

  Curso.find(query).skip(pagina * 10).limit(10)
       .then(cursos => {
         res.status(200).json({
           code: 0,
           message: cursos
         })
       }).catch(error => {
         console.log(error)
         res.status(500).json({
           code: 20,
           message: "Ocurrio un error con un módulo interno"
         })
       })
}

const postCursos = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
        code: 10,
        message: errors.array()
    })
  }
  
  const cursoNuevo = {}
  cursoNuevo.agnio = req.body.agnio
  cursoNuevo.duracion = req.body.duracion
  cursoNuevo.tema = req.body.tema
  cursoNuevo.alumnos = []

  Curso(cursoNuevo).save((error,  curso) => {
    if(error)
    {
      return console.log(error)
      res.status(500).json({
        code: 10,
        message: 'No se pudo crear el curso'
      })
    }
    res.status(201).json({
      code: 0,
      message: curso
    })
  })
}

const deleteCursos = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
        code: 10,
        message: errors.array()
    })
  }

  const curso = req.curso
  if(!curso)
        return res.status(404).json({
            code: 20,
            message: 'El curso no existe'
        })

  Curso.deleteOne(curso).then(ret => {
    res.status(200).json({
      code: 0,
      message: curso
    })
  }).catch(error => {
    return res.status(500).json({
      code: 10,
      message: 'Fallo un modulo interno'
    })
  })
}

module.exports = { getCursos, postCursos, deleteCursos }