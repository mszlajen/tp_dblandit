export default(
  function(name, value) {
    return {
      type: "EDIT",
      name,
      value
      }
  }
)