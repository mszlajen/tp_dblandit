import React from 'react' 
import changeWindow from './../redux/actions/changeWindow'
import {connect} from 'react-redux'
import handleNumericChange from '../helpers/handleNumericChange'
import handleNoValChange from '../helpers/handleNoValChange'
import stringifyParams from '../helpers/stringifyParams'
import { Container, Input, Button, Typography } from '@material-ui/core'
import Cursos from './Cursos'

const mapDispatchToProps = dispatch => {
  return {
    changeToMain: (main) => dispatch(changeWindow(main))
  }
}

class FormCurso extends React.Component {
  constructor() {
    super()
    this.state = {
      tema: "",
      duracion: "",
      agnio: "",
      errores: {}
    }
  }

  handleSave = (event) => {
    event.preventDefault()
    const params = stringifyParams({tema: this.state.tema, duracion: this.state.duracion, agnio: this.state.agnio})
    fetch("/cursos", {
      method:"POST", 
      body:params, 
      headers: {'Content-Type': 'application/json'}
      })
    .catch((e) => this.setState({errores: {server: "Hubo un problema con el servidor por favor intente nuevamente luego"}}))
    .then(this.handleFetchAnswer)
    
  }

  handleFetchAnswer = (fetchResult) => {
    switch(fetchResult.status)
    {
      case 400:
        const body = fetchResult.json()
        body.then(body => {
          const errores = {}
          body.message.forEach(e => errores[e.param] = e.msg)
          this.setState({errores})
        })
        break
      case 500:
        this.setState({errores: {server: "No se pudo crear el curso"}})
        break
      case 201:
        this.handleCancel()
        break
    }
  }

  handleCancel = () => {
    this.props.changeToMain(<Cursos />)
  }

  render() {
    return (
      <Container>
        <Typography>{this.state.errores.server}</Typography>
        <form method="POST">
          <Typography variant="error">{this.state.errores.tema}</Typography>
          <Input name="tema" placeholder="Tema" value={this.state.tema} onChange={handleNoValChange(this)}/>
          <br/>
          <Typography variant="error">{this.state.errores.duracion}</Typography>
          <Input type="numeric" placeholder="Duracion" name="duracion" value={this.state.duracion} onChange={handleNumericChange(this)}/>
          <br/>
          <Typography variant="error">{this.state.errores.agnio}</Typography>
          <Input type="numeric" placeholder="Año" name="agnio" value={this.state.agnio} onChange={handleNumericChange(this)}/>
          <br/>
          <Button onClick={this.handleSave}>Guardar</Button> 
          <Button onClick={this.handleCancel}>Cancelar</Button>
        </form>
      </Container>
    )
  }
}

export default connect(() => Object(), mapDispatchToProps)(FormCurso)