import React from 'react'
import {connect} from 'react-redux'
import {Container, Input, Button, Typography} from '@material-ui/core'
import handleNoValChange from '../helpers/handleNoValChange'
import handleNumericChange from '../helpers/handleNumericChange'
import stringifyParams from '../helpers/stringifyParams'
import addStudent from '../redux/actions/addStudent'

const mapDispatchToProps = dispatch => {
  return {
    add: (student, curse) => dispatch(addStudent(student, curse._id)) 
  }
}

class FormAlumno extends React.Component {
  constructor() {
    super()
    this.state = {
      nombre: "",
      apellido: "",
      dni: "",
      direccion: "",
      nota: "",
      errores: {}
    }
  }

  handleCreate = (event) => {
      const alumno = {nombre: this.state.nombre, apellido: this.state.apellido, dni: this.state.dni, direccion: this.state.direccion, nota: this.state.nota}
      event.preventDefault()
      fetch(`/cursos/${this.props.data._id}/alumnos`, {
        method: "POST",
        body: stringifyParams(alumno),
        headers: {'Content-Type': 'application/json'}
      })
      .catch((e) => this.setState({errores: {server: "Hubo un problema con el servidor por favor intente nuevamente luego"}}))
      .then(this.handleFetchAnswer(alumno))
    }
  
  handleFetchAnswer = (alumno) => (fetchResult) => {
    switch(fetchResult.status)
    {
      case 400:
        const body = fetchResult.json()
        body.then(body => {
          const errores = {}
          body.message.forEach(e => errores[e.param] = e.msg)
          this.setState({errores})
        })
        break
      case 500:
        this.setState({errores: {server: "No se pudo crear el alumno"}})
        break
      case 200:
        this.props.add(alumno, this.props.data)
        this.props.callback({currentTarget: {name: this.props.cancelName}})
        break
    }
  }
  render() {
    return (
      <Container>
        <Typography variant="error">{this.state.errores.server}</Typography>
        <form method="POST" >
          <Typography variant="error">{this.state.errores.nombre}</Typography>
          <Input placeholder="Nombre" name="nombre" value={this.state.nombre} onChange={handleNoValChange(this)}/>
          <br/>
          <Typography variant="error">{this.state.errores.apellido}</Typography>
          <Input placeholder="Apellido" name="apellido" value={this.state.apellido} onChange={handleNoValChange(this)}/>
          <br/>
          <Typography variant="error">{this.state.errores.dni}</Typography>
          <Input placeholder="DNI" name="dni" value={this.state.dni} onChange={handleNumericChange(this)}/>
          <br/>
          <Typography variant="error">{this.state.errores.direccion}</Typography>
          <Input placeholder="Direccion" name="direccion" value={this.state.direccion} onChange={handleNoValChange(this)}/>
          <br/>
          <Typography variant="error">{this.state.errores.nota}</Typography>
          <Input placeholder="Nota" name="nota" value={this.state.nota} onChange={handleNumericChange(this)}/>
          <br/>
          <Button onClick={this.handleCreate}>Crear</Button>
          <Button cancel={this.props.cancelName} onClick={this.props.callback}>Cancelar</Button>
        </form>
      </Container>
    )  
  }
}

export default connect(() => {}, mapDispatchToProps)(FormAlumno)