import React from 'react'
import Alumnos from './Alumnos'
import changeState from '../redux/actions/saveState'
import { useSelector, useDispatch } from 'react-redux'


function MejorAlumno(props) {
  const alumno = useSelector(s => s.state[`bestStudent${props.data}`] || [])
  const dispatch = useDispatch()
  fetch(`/cursos/${props.data}/mejorAlumno`)
  .then(result => result.json())
  .then(result => result.message && dispatch(changeState(`bestStudent${props.data}`, [result.message])))
  return (
    <Alumnos data={alumno} />
  )
}

export default MejorAlumno