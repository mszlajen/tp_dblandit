export default (
  function(state={}, action){
    switch(action.type){
      case "EDIT":
        return {
          ...state,
          [action.name]: action.value
        }
      default:
        return state
    }
  }
)