require('dotenv').config();

const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')

const app = express()

app.use(bodyParser.json())

const cursosRouter = require('./cursosCRUD/cursosRouter')
app.use('/cursos', cursosRouter)

app.get('/', (req, res) => res.send("Hello, it's working"))

const port = process.env.PORT || 8080
mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true })
        .then(() => {
            app.listen(port, () => console.log(`Escuchando en el puerto ${port}`)) 
        }).catch((err) => 
            console.log(err)
        )