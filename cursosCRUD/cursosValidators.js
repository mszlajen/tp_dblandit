const { check, param, checkSchema } = require('express-validator')
const Curso = require('../models/curso')
const Mongoose = require('mongoose')

const paginationValidators = [
  check('pagina').optional().isNumeric({no_symbols: true})
                 .withMessage('El numero de pagina debe ser un numero mayor a 0 (cero)')
]

const getIndexValidators = [
  check('agnio').optional().isNumeric({no_symbols: true})
                .withMessage('El año debe ser un numero')
                .custom(value => {if(parseInt(value) <= 0) 
                                    throw new Error('El año debe ser mayor a 0 (cero)')
                                  return true
                                  }),
  check('duracion').optional().isNumeric({no_symbols: true})
                   .withMessage('La duracion debe ser un numero')
                   .custom(value =>{if(parseInt(value) <= 0) 
                                      throw new Error('La duracion debe ser mayor a 0 (cero)')
                                    return true
                                    }),
              ]

const getOneValidators = [
  param('id').custom((id, {req}) => {
    return Curso.findById(Mongoose.Types.ObjectId(id)).catch(err =>{
        console.log(err)
        throw new Error('Fallo un modulo interno')
      }).then(function(curso) {
        req.curso = curso
        return true
      })
  })
]

const postValidators = [
  checkSchema({
    agnio: {
      in: ['body'],
      errorMessage: 'El año no es valido',
      isNumeric: true,
      toInt: true
    },
    duracion: {
      in: ['body'],
      errorMessage: 'La duracion no es valido',
      isNumeric: true,
      toInt: true
    },
    tema: {
      in: ['body'],
      errorMessage: 'Debe tener un tema',
      exists: true
    }
  })
]

module.exports = {paginationValidators, getIndexValidators, getOneValidators, postValidators}