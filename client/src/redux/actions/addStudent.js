export default (
    (student, curse) => {
        return {
            type: "APPEND",
            payload: {student, curse}
        } 
    }
)