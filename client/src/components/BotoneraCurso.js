import React from 'react'
import Alumnos from './Alumnos'
import MejorAlumno from './MejorAlumno'
import EliminarCurso from './EliminarCurso'
import FormAlumno from './FormAlumno'
import { Button, Container, ButtonGroup } from '@material-ui/core'

class BotoneraCurso extends React.Component {
  constructor() {
    super()
    this.state = {
      showing: "buttons"
    }
  }

  handleClick = (event) => {
    const {name} = event.currentTarget
    this.setState({showing: name})
  }

  render() {
  let show;
  switch(this.state.showing)
  {
    case "buttons":
      show = (
        <ButtonGroup>
          <Button name="create" onClick={this.handleClick}>Crear Nuevo</Button>
          <Button name="students" onClick={this.handleClick}>Ver Alumnos</Button>
          <Button name="bestStudent" onClick={this.handleClick}>Ver Mejor Alumno</Button>
          <Button name="delete" onClick={this.handleClick}>Eliminar</Button>
        </ButtonGroup>
      )
      break
    case "create":
      show= (
        <Container>
          <FormAlumno data={this.props.data} callback={this.handleClick} cancelName="buttons"/>
        </Container>
      )
      break
    case "students":
      show= (
        <Container>
          <Alumnos data={this.props.data.alumnos}/>
          <Button name="buttons" onClick={this.handleClick}>Esconder</Button>
        </Container>
      )
      break
    case "bestStudent":
      show= (
        <Container>
          <MejorAlumno />
          <Button name="buttons" onClick={this.handleClick}>Esconder</Button>
        </Container>
      )
      break
    case "delete":
      show=(
        <Container>
          <EliminarCurso data={this.props.data} callback={this.handleClick} cancelName="buttons" />
        </Container>
      )
      break
    default:
      show=<h1>Entro acá</h1>
  }

  return show
  }
}

export default BotoneraCurso