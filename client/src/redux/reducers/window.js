import React from 'react'
import Cursos from '../../components/Cursos'

export default (function(state = <Cursos />, action) {
  if(action.type === "CHANGE")
    return action.payload
  return state
})