import React from 'react'
import { Table, Container, TableHead, TableCell, TableRow, TableBody} from '@material-ui/core'

function Alumnos(props){
  const alumnos = props.data.map(alumno => {
      return (
              <TableRow>
                <TableCell>{alumno.nombre}</TableCell>
                <TableCell>{alumno.apellido}</TableCell>
                <TableCell>{alumno.dni}</TableCell>
                <TableCell>{alumno.direccion}</TableCell>
                <TableCell>{alumno.nota}</TableCell>
              </TableRow>
              )
  })
  return (
    <Container>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Nombre</TableCell>
            <TableCell>Apellido</TableCell>
            <TableCell>DNI</TableCell>
            <TableCell>Direccion</TableCell>
            <TableCell>Nota</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {alumnos}
        </TableBody>
      </Table>
    </Container>
  )
}

export default Alumnos