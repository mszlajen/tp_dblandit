const { checkSchema } = require('express-validator')

const postValidators = [
    checkSchema({
        nombre: {
            in: ['body'],
            errorMessage: 'Debe tener un nombre',
            exists: true
        },
        apellido: {
            in: ['body'],
            errorMessage: 'Debe tener un apellido',
            exists: true
        },
        direccion: {
            in: ['body'],
            errorMessage: 'Debe tener un direccion',
            exists: true
        },
        dni: {
            in: ['body'],
            errorMessage: 'El dni no es valido',
            isNumeric: true,
            toInt: true
        },
        nota: {
            in: ['body'],
            errorMessage: 'La nota no es valido',
            isNumeric: true,
            toInt: true
        }
    })
]

module.exports = { postValidators }