export default (
  function(payload) {
    return {
      type: "DELETE",
      payload
    }
  }
)