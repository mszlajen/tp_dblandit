const Curso = require('../models/curso')
const { validationResult } = require('express-validator')

const getAlumnos = (req, res, next) => {
    const curso = req.curso
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
        return res.status(400).json({
            code: 10,
            message: errors.array()
        })
    }

    if(curso)
        return res.status(200).json({
            code: 0,
            message: curso.alumnos
        })

    return res.status(404).json({
        code: 20,
        message: 'El curso no existe'
    })
}

const getMejorAlumno = (req, res, next) => {
    const curso = req.curso
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
        return res.status(400).json({
            code: 10,
            message: errors.array()
        })
    }

    if(!curso)
        return res.status(404).json({
            code: 20,
            message: 'El curso no existe'
        })

    const notaMaxima = Math.max(curso.alumnos.map(a => a.nota))
    const mejorAlumno = curso.alumnos.find(a => a.nota = notaMaxima)

    return res.status(200).json({
        code: 0,
        message: mejorAlumno
    })
}

const postAlumnos = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({
            code: 10,
            message: errors.array()
        })
    }

    const alumno = {}
    /*for(k in ['nombre', 'apellido', 'dni', 'direccion', 'nota'])
        alumno[k] = req.query[k]*/
    alumno.nombre = req.body.nombre
    alumno.apellido = req.body.apellido
    alumno.direccion = req.body.direccion
    alumno.nota = req.body.nota
    alumno.dni = req.body.dni

    const curso = req.curso
    if(!curso)
        return res.status(404).json({
            code: 20,
            message: 'El curso no existe'
        })
    
    curso.alumnos.push(alumno)

    Curso(curso).save((error,  curso) => {
        if(error)
        {
            return console.log(error)
            res.status(400).json({
            code: 10,
            message: 'No se pudo agregar al alumno'
            })
        }
        res.status(200).json({
            code: 0,
            message: curso
        })
    })
}

module.exports = { getAlumnos, getMejorAlumno, postAlumnos }