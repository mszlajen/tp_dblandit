export default (function(payload) {
  return {
    type: "CHANGE",
    payload
  }
})