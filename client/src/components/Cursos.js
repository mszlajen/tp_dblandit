import React from 'react'
import Curso from './Curso'
import {connect} from 'react-redux'
import changeWindow from '../redux/actions/changeWindow'
import handleNumericChange from '../helpers/handleNumericChange'
import searchCurses from '../helpers/searchCurses'
import FormCurso from './FormCurso'
import {Input, Button, Paper, Container} from '@material-ui/core'

const mapStateToProps = state => {
  return {
    cursos: state.curses
  }
}

const mapDispatchToProps = dispatch => {
  return {
    search: (params) => () => searchCurses(dispatch, params),
    changeWindow: (window) => () => dispatch(changeWindow(window))
  }
}

class Cursos extends React.Component {
  constructor() {
    super()
    this.state = {
      duracion: "",
      agnio: "",
      pagina: ""
    }
  }

  render() {
    const paginaActual = this.state.pagina
    const cursosComponents = this.props.cursos.map(curso => <div><Curso key={curso["_id"]} data={curso} /><br></br></div>)
  
    return (
      <Container>
        <form>
          <Input type="numeric" name="duracion" placeholder="duracion" value={this.state.duracion} onChange={handleNumericChange(this)} />
          <Input type="numeric" name="agnio" placeholder="año" value={this.state.agnio} onChange={handleNumericChange(this)} />
          <br/>
          <Button variant="contained" onClick={this.props.search({...this.state, pagina: 0})}>Search</Button>
        </form>
        <Paper>
          {cursosComponents}
        </Paper>
        <Button variant="contained" onClick={this.props.changeWindow(<FormCurso return={this}/>)}>Crear Nuevo</Button>
        {paginaActual > 0 && <Button variant="contained" onClick={this.props.search({...this.state, pagina: paginaActual - 1})}>Pagina anterior</Button>}
        <Button variant="contained" onClick={this.props.search({...this.state, pagina: paginaActual + 1})}>Pagina siguiente</Button>
      </Container>
    )

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cursos)