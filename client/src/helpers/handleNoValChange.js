export default (context) => {
  return (event) => {
          const {name, value} = event.target
          context.setState({[name]: value})
        }
}