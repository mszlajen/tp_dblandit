import React from 'react';
import './App.css';
import search from './helpers/searchCurses'
import {connect} from 'react-redux'
import {AppBar, Container, Typography} from '@material-ui/core'

const mapStateToProps = state => {
  return {
    window: state.window
  } 
}

const mapDispatchToProps = dispatch => {
  return {
    search: () => search(dispatch)
  }
}


class App extends React.Component{
  componentDidMount() {
    this.props.search()
  }

  render() {
    return (
      <div className="App">
        <AppBar position="static">
          <Typography>Administrador de cursos</Typography>
        </AppBar>
        <Container component="main">
          {this.props.window}
        </Container>
      </div>
    );

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
