export default (o) => {
    const keys = Object.keys(o)
    const params = {}
    keys.filter(k => o[k] !== "").forEach(k => params[k] = o[k])
    return JSON.stringify(params)
}