import React from 'react'
import BotoneraCurso from './BotoneraCurso'
import {Card, CardContent, Typography} from '@material-ui/core'

function Curso(props){ 
  return (
    <Card raised={true}>
        <CardContent>
          <Typography gutterBottom={true} align="center">{props.data.tema}</Typography>  
          <Typography align="center">Duracion: {props.data.duracion}</Typography>
          <Typography align="center">Año: {props.data.agnio}</Typography>
        </CardContent>
        {<BotoneraCurso data={props.data}/>}
    </Card>)
}

export default Curso