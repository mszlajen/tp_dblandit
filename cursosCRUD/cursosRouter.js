const cursosRouter = require('express').Router()

const { getCursos, postCursos, deleteCursos } = require('./cursosController')
const { paginationValidators, getIndexValidators, getOneValidators, postValidators } = require('./cursosValidators')
const { getAlumnos, getMejorAlumno, postAlumnos } = require('../alumnosCRUD/alumnosController')
const alumnosPostValidators = require('../alumnosCRUD/alumnosValidators').postValidators

cursosRouter.get('/', paginationValidators, getIndexValidators, getCursos)

cursosRouter.post('/', postValidators, postCursos)

cursosRouter.delete('/:id', getOneValidators, deleteCursos)

cursosRouter.post('/:id/alumnos', getOneValidators, alumnosPostValidators, postAlumnos)

cursosRouter.get('/:id/alumnos', getOneValidators, getAlumnos)

cursosRouter.get('/:id/mejorAlumno', getOneValidators, getMejorAlumno)

module.exports = cursosRouter